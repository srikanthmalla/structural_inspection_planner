# INSPECTION PLANNER #

1.download structural inspection planner source code
```
git clone git@bitbucket.org:srikanthmalla/structural_inspection_planner.git
```
2.after running
```
roslaunch koptplanner hoaHakanaia.launch
rosrun request hoaHakanaia
```
3.it logs the path to the file below
```
koptplanner/data/latestPath.csv
```
4.you can visualize path and mesh using the files in matlab
```
request/visualization/inspectionPathVisualization.m  %matlab code for visualization
request/visualization/inspectionScenario.m   % path and mesh data
```
5.Planning code is found at:
```
/koptplanner/src/plan.cpp
```
## Changing number of traingles in the mesh ##

1.download ACVD : Surface Mesh Coarsening and Resampling zip file from the website

[ACVD](http://www.creatis.insa-lyon.fr/site/en/acvd)

2.unzip and go to it's directory,

** NOTE:copy your stl or obj or ply mesh file here **
```
/.ACVD <yourmeshfilename.ext>
```
3.It will ask for
```
Number of vertices ? your expected no of vertices
Gradation ? 1
```
4.output is stored in ply format with name output_1.ply

5.converting ply to stl file, go to the link given below

http://www.greentoken.de/onlineconv/

or you can use meshlab

## Editing configuration file of inspection planner ##

1.As the range can be changed in the plan.cpp file
```
tri_t::setParam(req.incidenceAngle, req.minDist, req.maxDist); //change the params
```
2.
```
```

## Changing facets direction ##

By changing the direction of the facets you could generate the way-points inside the model also.

1.open meshlab

2.load the original mesh

3.Filters -> Normals, Curvature and Orientation -> Invert Faces Normals -> Apply